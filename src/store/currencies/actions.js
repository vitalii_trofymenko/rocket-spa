export default {
    getCurrenciesRates({ commit }, { params }) {
        return this.$axios.get('/currencies/rates', { params: params }).then((response) => {
            commit('setCurrenciesRates', response.data)
        })
    },
    getCurrencyRates({ commit }, { id, params }) {
        return this.$axios.get(`/currencies/${id}/rates`, { params: params }).then((response) => {
            commit('setCurrencyRates', response.data)
        })
    }
}
