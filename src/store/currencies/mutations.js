export default {
    setCurrenciesRates(state, payload) {
        state.currenciesRates = payload
    },
    setCurrencyRates(state, payload) {
        state.currencyRates = payload
    },
    clearCurrencyRates(state) {
        state.currencyRates = null
    }
}
