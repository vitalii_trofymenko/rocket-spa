export default {
    login({ dispatch }, { payload }) {
        return this.$axios.post('auth/login', payload).then((response) => {
            localStorage.setItem('token', response.data.data.token);
        })
    },
    checkTokenExists() {
        const token = localStorage.getItem('token');
        if (!token) {
            return Promise.reject(new Error('NO_STORAGE_TOKEN'))
        }
        return Promise.resolve(token)
    },
    getAuthUser({ commit }) {
        return this.$axios.get('auth/user').then((response) => {
            commit('setUser', response.data.data)
        })
    },
    logout({ dispatch }) {
        return this.$axios.post('auth/logout').then((response) => {
            return dispatch('clearAuth')
        })
    },
    clearAuth({ commit }) {
        localStorage.removeItem('token');
        commit('clearAuth')
    }
}
