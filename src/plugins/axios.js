import ax from 'axios'

export const axios = ax.create({
    baseURL: 'http://18.195.169.226:81/api/v1'
});

axios.interceptors.request.use(
    function(config) {
        const token = localStorage.getItem('token');
        if (token != null) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    function(error) {
        return Promise.reject(error.response);
    }
);
